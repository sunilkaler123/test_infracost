# Infracost GitLab CI Demo

See [this merge request](https://gitlab.com/infracost/gitlab-ci-demo/-/merge_requests/4) for the demo.

The [Infracost GitLab-CI template](https://gitlab.com/infracost/infracost-gitlab-ci/) runs [Infracost](https://infracost.io) against pull requests. It automatically adds a pull request comment showing the cost estimate difference for the planned state if a configurable percentage threshold is crossed.

See the [Infracost integrations](https://www.infracost.io/docs/integrations/cicd) page for other integrations.
